#!/bin/sh
#echo "Number of args:$#"
if test $# -ne 3 #print help if nuber of parametrs less of 3
then echo "Usage: pins.sh <pin> <direction> <value>"
echo "<pin> - number_of_pin = (pin + bank_of_pin*32). See IMX23RM.pdf list-1426 (Table 37-3. Pin Multiplexing for 128-Pin QFP Packages)"
echo "<direction> - pin derection: '0' - input, '1' - output"
echo "<value> - pin state: '0' - low level, '1' - high lewel"
exit 2
fi

(ls /sys/class/gpio/ | grep  -w "gpio$1") || (echo $1 > /sys/class/gpio/export)

if test $2 -eq 0
then (ls /sys/class/gpio/gpio$1/ | grep  -w "direction") && (echo "in" > /sys/class/gpio/gpio$1/direction)
cat /sys/class/gpio/gpio$1/direction
echo "pin_value:"
cat /sys/class/gpio/gpio$1/value
else (ls /sys/class/gpio/gpio$1/ | grep  -w "direction") && (echo "out"> /sys/class/gpio/gpio$1/direction)
cat /sys/class/gpio/gpio$1/direction
  if test $3 -eq 0
  then echo "0"> /sys/class/gpio/gpio$1/value
  else echo "1"> /sys/class/gpio/gpio$1/value
  fi
echo "pin_value:"
cat /sys/class/gpio/gpio$1/value
fi

