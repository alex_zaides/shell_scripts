#!/bin/sh
#echo $#
#echo $@

#find substring in string
# $1 - substring, $2..$n - string 
strstr () {
	#delete firs argument from string
	out=`echo $@ | sed "s/$1//"` 
	#find substing in string
	echo $out | grep -w $1 > /dev/null && return 0
	return 1
}

######### Information section
if [ $# -eq 0 ] ; then
    echo "IKv3 test programm v1.0"
    echo "Usage: $0 [-atvf] [FILE] {test number}"
    echo "Options:"
    echo "                          Print this usage"
    echo "      -a                  Execute all test"
    echo "      -t                  Print list of tests"
    echo "      -t {test number}    Execute test number {test number}"
    echo "      -v                  Verbose log mode"
    echo "      -f [FILE]           [FILE] - log file"
    echo "Example: $0 -a -f /mnt/testIKv3.log"
    exit 0;
fi

prev_arg=0
test_num=0
verbose=0
logfile=/dev/null

#pars args
for arg in $@ ; do
    case "$arg" in
        -a)
            test_num=all ;;
        -v)
            verbose=1 ;;
        -t) echo $test_num | grep -q "all" || test_num=list ;;
        -f) ;;
        *)
            case "$prev_arg" in
                -f) logfile=$arg ;;
                -t)  
                    if [ $test_num !=  "all" ] ; then
                        test_num=$arg;
                    fi           
					;;
            esac
    esac
	prev_arg=$arg
done

test_list="ttyS2 ttyS4 ttyS3 analog(4-20) pulse_inputs modem(csq,ccid,cpas,ballance) usb_flash kernel_test(free,df,cpuload) Voltage_Hardware_real_time_clock STMU"


echo "`date +\"%D %T\"` $0" > $logfile 2>&1

#echo "test_num:$test_num"
#echo "verbose:$verbose"
echo "logfile:$logfile"
#echo " "

#Printing test list if no args after -t 
if [ $test_num = "list" ] ; then
	test_number=1
	for test in $test_list ; do
		echo "$test_number. $test"
		let "test_number=test_number+1"
	done
	exit 0;
fi

if [ $test_num = "1" ] || [ $test_num = "all" ] ; then
	echo "Test number: 1"
	port=/dev/ttyS2
	test_str=123456
	echo_str=`/mnt/scripts/net_utils/read_mdm.x -s 115200 -p $port -c $test_str -t 0`
	echo $echo_str | grep $test_str >> $logfile 2>&1 && result=OK || result=FAIL
	echo "    Testing RX/TX sygnals on $port port...$result"

	tmp_file=/tmp/${0##*/}.tmp
	/mnt/scripts/net_utils/read_mdm.x | grep "cts" >> $logfile 2>&1 || { echo "/mnt/scripts/net_utils/read_mdm.x - no support RTS/CTS control" ; exit 1 ; }
	/mnt/scripts/net_utils/read_mdm.x -s 115200 -p $port -c $test_str -t 0 -cts > $tmp_file &
	pid=$!
	sleep 3
	ps | grep $pid | grep -v grep >> $logfile 2>&1 && kill $pid
	result=FAIL
	if [ -f $tmp_file ] ; then 
		cat $tmp_file | grep $test_str >> $logfile 2>&1 && result=OK
		rm $tmp_file >> $logfile 2>&1
	fi
	echo "    Testing CTS/RTS sygnals on $port port...$result"
fi

if [ $test_num = "2" ] || [ $test_num = "all" ] ; then
	echo "Test number: 2"
	port=/dev/ttyS4
	test_str=123456
	echo_str=`/mnt/scripts/net_utils/read_mdm.x -s 115200 -p $port -c $test_str -t 0`
	echo $echo_str | grep $test_str >> $logfile 2>&1 && result=OK || result=FAIL
	echo "    Testing RX/TX sygnals on $port port...$result"

	tmp_file=/tmp/${0##*/}.tmp
	/mnt/scripts/net_utils/read_mdm.x | grep "cts" >> $logfile 2>&1 || { echo "/mnt/scripts/net_utils/read_mdm.x - no support RTS/CTS control" ; exit 1 ; }
	/mnt/scripts/net_utils/read_mdm.x -s 115200 -p $port -c $test_str -t 0 -cts > $tmp_file &
	pid=$!
	sleep 3
	ps | grep $pid | grep -v grep >> $logfile 2>&1 && kill $pid
	result=FAIL
	if [ -f $tmp_file ] ; then 
		cat $tmp_file | grep $test_str >> $logfile 2>&1 && result=OK
		rm $tmp_file >> $logfile 2>&1
	fi
	echo "    Testing CTS/RTS sygnals on $port port...$result"
fi

if [ $test_num = "3" ] || [ $test_num = "all" ] ; then
	echo "Test number: 3"
	timeout=15
	port=/dev/ttyS3
	exec 3<$port
	stty -F "$port" 115200 rs485 "-echo"
	echo "sending message to $port speed 115200" 
	echo "Test message to check if RS485 is working is sent" > $port
	read -t 15 -u 3 #clear echo
	read -t 1 -u 3 #clear echo \n symbol
	echo "Waiting for input message. Timeout is $timeout"
	read -t $timeout -u 3 answer && {
		echo "received message: $answer"
		result=OK
	} || {
		echo "timeout while getting answer"
		result=FAILED
	}
	echo "    Testing RS485 on $port port...$result"
fi

if [ $test_num = "4" ] || [ $test_num = "all" ] ; then
	echo "Test number: 4"
	answer=`/mnt/msp/adc_test`
	for channel in 0 1 2 3 4
	do
		value=`echo "$answer" | sed -n "s/CHANNEL \[$channel] = //p"` && {
		result=FAILED
		if [ -n "$value" ]
		then
			if [ "$value" -lt "1740" -a "$value" -ge "1690"  ]
			then
				result=OK
				let "value_ma = (value * 24) / 4095"
			fi
		fi
		echo "    Testing ADC channel $channel ...$value=$value_ma mA $result"
	}
	done
fi

if [ $test_num = "5" ] || [ $test_num = "all" ] ; then
	echo "Test number: 5"
	answer=`/mnt/msp/ioctl_test -t` && result=OK || result=FAILED
	value=`echo "$answer" | sed -n "s/P[1-5] COUNT = //p"`
	echo value=$value
		if [ $result = "OK" ]
	then
		sleep 10
		answer=`/mnt/msp/ioctl_test -t` && result=OK || result=FAILED 
		new_value=`echo "$answer" | sed -n "s/P[1-5] COUNT = //p"`
	fi
	echo new_value=$new_value
	if [ $result = "OK" ]
	then
		for channel in 1 2 3 4 5
		do
			result=FAILED
			val=`echo $value | awk "{print \\$$channel}"`
			new_val=`echo $new_value | awk "{print \\$$channel}"`
			let "delta = new_val - val"
			if [ -n "$val" -a -n "$new_val" -a "$delta" -le 15 -a $delta -ge 5 ]
			then
				result=OK
			fi
			echo "    Testing pulse inputs. channel $channel ...old=$val, new=$new_val, delta=$delta $result"
		done
	else
		echo "    Testing pulse inputs ....  Can't connect MSP. $result"
	fi
fi

if [ $test_num = "6" ] || [ $test_num = "all" ] ; then
	echo "Test number: 6"
	rate=115200
	port=/dev/ttyS1
	
	for sim in 1 2; do
		result=OK
		set 0; set `ps | grep gprs1 | grep -v grep | grep -v "${0##*/}"` >> $logfile 2>&1
		if [ "$1" -ne "0" ]
		then
			echo "$1 killed" >> $logfile 2>&1
			kill $1
			sleep 5
		fi
		echo "    Switch to SIM:$sim"
		/mnt/scripts/net_utils/rstmdm.sh sim$sim >> $logfile 2>&1
		sleep 15
		repl=`/mnt/scripts/net_utils/read_mdm.x -s $rate -p $port -c AT+CSQ -t 0 | head -n 3 | tail -n 1`
		repl="$req: $repl"
		echo "    Signal quality: ${repl##*:}" 
		reply_int=${repl##*:} 
		if [ -n $reply_int ] ;then
			let "reply_int=${reply_int%%,*}"
		fi
		if [ $reply_int -lt 8 ] ; then
		    result=FAIL
		fi
		repl=`/mnt/scripts/net_utils/read_mdm.x -s $rate -p $port -c AT+CCID -t 0 | grep -v "OK" | grep -v "AT" | grep -v "^[[:space:]]*$"`
		repl=`echo "$repl" | sed "s/ERROR.*/ERROR/"`
		echo "    CCID sim: $repl"
		if [ "$repl" = "ERROR" ] ;then
			result=FAIL
		fi
		repl=`/mnt/scripts/net_utils/read_mdm.x -s $rate -p $port -c AT+CPAS -t 0 | head -n 3 | tail -n 1`
		repl="$req: $repl"
		echo "    Network registration: ${repl##*:}"
		reply_int=${repl##*:} 
		if [ -n $reply_int ] ;then
			let "reply_int=${reply_int%%,*}"
		fi
		if [ $reply_int -ne 0 ] ; then
		    result=FAIL
		fi
		echo "    Modem test with SIM$sim:...$result"
	done	
fi

if [ $test_num = "7" ] || [ $test_num = "all" ] ; then 
	echo "Test number: 7"
	is_mount=0
	for flash_drive in /dev/sd* ; do
	    mkdir -p /mnt/$flash_drive >> $logfile 2>&1
		mount $flash_drive /mnt/$flash_drive >> $logfile 2>&1 && { echo "    Mounting flash drive:$flash_drive...OK" ; is_mount=1 ; umount $flash_drive ; }
	done
	rm -r /mnt/dev >> $logfile 2>&1
	if [ $is_mount = 0 ] ; then
		echo "    Mounting flash drive...FAIL"	
	fi
fi

if [ $test_num = "8" ] || [ $test_num = "all" ] ; then
	result=OK
	echo "Test number: 8"
	MemTotal=`cat /proc/meminfo | head -n 1 | sed -e "s/\(.*MemTotal:\)\(.*\)\(kB.*\)/\2/"`
	MemFree=`cat /proc/meminfo | head -n 2 | tail -n 1 | sed -e "s/\(.*MemFree:\)\(.*\)\(kB.*\)/\2/"`
	let "tmp=MemTotal/100"
	let "MemFreePercent=MemFree/tmp"
	echo "    IK ram free...$MemFreePercent%"
	Loadavg=`cat /proc/loadavg`
	Loadavg=${Loadavg%% *}
	echo "    IK cpu load...${Loadavg##*.}%"
	upt=`uptime`
	upt=`echo $upt | sed -e "s/\(.*up \)\(.*\)\(, load.*\)/\2/"`
	echo "    IK uptime...$upt"
	echo "    IK filesystem parts:"
	df
	echo " "
	let "Loadavg=${Loadavg##*.}"
	if [ $MemFreePercent -le 50 ] || [ $Loadavg -ge 50 ] ; then
		result=FAIL
	fi
	echo "    IK system test...$result"	
fi

if [ $test_num = "9" ] || [ $test_num = "all" ] ; then
	echo "Test number: 9"
	hwclock && result=OK || result=FaLED
	echo "    IK hwclock test...$result"
fi

if [ $test_num = "10" ] || [ $test_num = "all" ] ; then
	echo "Test number: 10"
fi

exit 0;