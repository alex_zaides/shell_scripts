#!/bin/sh

selfname=${0##*/}
kill -9 `pidof -o $$ $selfname`
led="/mnt/scripts/pins.sh $1 1"

case "$2" in
	1)
		$led 1 > /dev/null
	;;
	2)
		while [ 1 ] 
		do
			$led 0 > /dev/null
			usleep 100000
			$led 1 > /dev/null
			usleep 100000
		done
	;;
	*) 
		$led 0 > /dev/null
	;;
esac