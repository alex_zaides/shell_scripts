#!/bin/sh
echo "started." | logger -t "net-set $0[$$]"

started="/etc/ppp/started"
log="/etc/ppp/log"

if [ $# -lt 2 ] ; then
    echo "GPRS launcher v1.0"
    echo "Usage: $0 [gprs1|gprs2] [server]"
	exit 1
fi

echo "gprs1 gprs2" | grep -w "$1" > /dev/null 2>&1 || { 
    echo "Connection name:$1 not supported" | logger -t "net-set $0[$$]" 
    exit 0
}

selfname=${0##*/}
selfpid=$$
connection=$1
server=$2
server2=$3

#for a in  `ps | grep "cat" | grep -v "grep" | sed 's/ root.*//' | grep -v 7937` ; do

#test connection
for ping_serv in $server $server2 ; do
	for a in 1 2 3
	do
	  if ping -c 1 $ping_serv > /dev/null 2>&1 ; then
		echo "first pings OK. Exiting." | logger -t "$selfname[$selfpid]"
		exit 0 
	  fi
	done
done
	
#kill connection
set 0; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null; test $1 -eq 0 || kill $1
echo "`date +\"%D %T\"` $selfname: PID:$1" | logger -t "net-set $0[$$]" 
#check modem
/etc/ppp/checkmodem.sh `echo $connection | sed "s/gprs/sim/"` || ( echo "Modem Failed. Exiting." | logger -t "$selfname[$selfpid]" && exit 1 )
sleep 5

for a in 1 2
do
  #start connection
  rm $started > /dev/null 2>&1
  rm $log > /dev/null 2>&1
  pppd call $connection > $log &
  count=0
  while [ "$count" -le 100 ]      
  do
    grep $connection $started > /dev/null 2>&1 && echo "$connection started OK###." | logger -t "$selfname[$selfpid]" && exit 0
    sleep 1
    let "count += 1"  
  done
  #kill connection
  set 0; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null; test $1 -eq 0 || kill $1
done
echo "$connection start FAILED." | logger -t "$selfname[$selfpid]"
exit 1
