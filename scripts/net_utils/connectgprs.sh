#!/bin/sh
echo "started with $1." | logger -t "net-set $0[$$]"

started="/tmp/ppp/started"
log="/tmp/ppp/log"

if [ $# -lt 1 ] ; then
    echo "GPRS launcher v1.0" | logger -t "net-set $0[$$]" 
    echo "Usage: $0 [gprs1|gprs2]"| logger -t "net-set $0[$$]" 
	exit 1
fi

echo "gprs1 gprs2" | grep -w "$1" > /dev/null 2>&1 || { 
    echo "Connection name:$1 not supported" | logger -t "net-set $0[$$]" 
    exit 0
}

selfname=${0##*/}
selfpid=$$
connection=$1

	
#kill connection
set 0; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null; test $1 -eq 0 || kill $1
echo "`date +\"%D %T\"` $selfname: killed process PID:$1" | logger -t "net-set $0[$$]" 
#check modem
/etc/ppp/checkmodem.sh `echo $connection | sed "s/gprs/sim/"` || {
	echo "Modem Failed. Exiting." |  logger -t "net-set $0[$$]"
	exit 1 
}
sleep 5

for a in 1 2
do
  #start connection
  rm $started > /dev/null 2>&1
  rm $log > /dev/null 2>&1
  pppd call $connection > $log &
  count=0
  while [ "$count" -le 100 ]      
  do
    grep $connection $started > /dev/null 2>&1 && echo "$connection started OK###." | logger -t  logger -t "net-set $0[$$]" && exit 0
    sleep 1
    let "count += 1"  
  done
  #kill connection
  set 0; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null; test $1 -eq 0 || kill $1
done
echo "$connection start FAILED." | logger -t "net-set $0[$$]"
exit 1
