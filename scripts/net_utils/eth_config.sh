#!/bin/sh

######### Information section
if [ $# -lt 3 ] ; then
	echo "Invalid number of args"
    echo "Script sets network settings"
    echo "Usage: $0 address netmask mac [gateway] "
    exit 0;
fi

target_file=/etc/network/interfaces

sed -i "s/^address .\+/address $1/" $target_file 
sed -i "s/netmask .\+/netmask $2/" $target_file
sed -i "s/hwaddress ether .\+/hwaddress ether $3/" $target_file

if [ -n "$4" ] ;then 
	grep "gateway" $target_file > /dev/null 2>&1 && (sed -i "s/gateway.\+/gateway $4/" $target_file) || echo "gateway $4" >> $target_file	
else
	sed	-i "/gateway.\*/ d" $target_file
fi

#ifdown -a uncomment to change settings automaticalli now
#ifup -a 