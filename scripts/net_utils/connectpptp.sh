#!/bin/sh
echo "started." | logger -t "net-set $0[$$]"

started="/tmp/ppp/started"
log="/tmp/vpn/log"

selfpid=$$
selfname=${0##*/}

for connection in "pptp1" "pptp2"
do
	#kill connection
	set 0 ; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null ; test $1 -eq 0 || kill $1
	echo "`date +\"%D %T\"` $selfname: killed process PID:$1" | logger -t "net-set $0[$$]"
	
	echo "$connection started connection" | logger -t "net-set $0[$$]"
	
	for a in 1 2
	do
	  #start connection
		rm $started >/dev/null 2>&1
		rm $log >/dev/null 2>&1
		pppd call $connection > $log &
	
		count=0
		while [ "$count" -le 100 ]      
		do
		   grep $connection $started > /dev/null 2>&1 && echo "$connection started OK###." | logger -t "net-set $0[$$]" && exit 0
		   sleep 1
		   let "count += 1"  
		done
	
	  	#kill connection
		set 0; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null; test $1 -eq 0 || kill $1
	
	done
	echo "$connection start FAILED." | logger -t "net-set $0[$$]"
done
exit 1
