#!/bin/sh
echo "started." | logger -t "net-set $0[$$]"

started="/etc/ppp/started"
log="/etc/ppp/log"

if [ $# -lt 2 ] ; then
    echo "PPTP launcher v1.0"
    echo "Usage: $0 [pptp1|pptp2] [server]"
	exit 1
fi

echo "pptp1 pptp2" | grep -w "$1" || { 
    echo "Connection name:$1 not supported" | logger -t "net-set $0[$$]" 
    exit 0
}

selfpid=$$
selfname=${0##*/}
connection=$1
server=$2

#for a in  `ps | grep "cat" | grep -v "grep" | sed 's/ root.*//' | grep -v 7937` ; do

#test connection
for a in 1 2 3
do
  if ping -c 1 $server ; then
	echo "first pings OK. Exiting." | logger -t "$selfname[$selfpid]"
	exit 0 
  fi
done

#kill connection
set 0 ; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null ; test $1 -eq 0 || kill $1
echo "PID:$1 #"

for a in 1 2
do
  #start connection
  rm $started
  rm $log
  pppd call $connection > $log &

  count=0
  while [ "$count" -le 100 ]      
  do
    grep $connection $started > /dev/null 2>&1 && echo "$connection started OK###." | logger -t "$selfname[$selfpid]" && exit 0
    sleep 1
    let "count += 1"  
  done

  #kill connection
  set 0; set `ps | grep "$connection" | grep -v grep | grep -v "$selfname"` > /dev/null; test $1 -eq 0 || kill $1

done
echo "$connection start FAILED." | logger -t "$selfname[$selfpid]"
exit 1
