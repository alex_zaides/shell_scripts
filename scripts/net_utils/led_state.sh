#!/bin/sh

selfname=${0##*/}
kill -9 `pidof -o $$ $selfname`
led="/mnt/scripts/pins.sh 152 1"

$led 1 > /dev/null 2>&1

while [ 1 ]
do
	sleep 2
	count=0
	while [ "$count" -lt $1 ]      
	do
		$led 0 > /dev/null 2>&1
		usleep 50000
		$led 1 > /dev/null 2>&1
		usleep 100000
		let "count += 1"  
	done
done
