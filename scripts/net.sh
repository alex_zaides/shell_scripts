#Create net.sh script
#Check if net.sh already running

if [ `ps | grep -v '/bin/sh'| grep -c 'net.s[h]'` -gt 2 ]
then
	echo "Try starting net.sh. Already running" | logger -t "net-set $0[$$]"
	exit 0
fi


prior=`echo 'sim2;eth0;sim1' | sed 's/;/ /g'`
if [ ! -f /tmp/netsh.dat ]; then
	echo "File /tmp/netsh.dat not found. Writing default settings" | logger -t "net-set $0[$$]"
	echo NULL > /tmp/netsh.dat			#current channel try to connect
	echo "`date`" >> /tmp/netsh.dat	#time working channel was set
	echo NULL >> /tmp/netsh.dat			#last working channel
fi

mkdir -p /tmp/ppp

#get current channel
working_channel="`head -n 3 /tmp/netsh.dat | tail -n 1`"

for i in "1" "2"
do
	if [ ! -f /tmp/vpn/down_pptp$i ]; then
		echo "File /tmp/vpn/down_pptp$i not found. Copy from /etc/vpn/disconnect_pptp$i" | logger -t "net-set $0[$$]"
		mkdir -p /tmp/vpn
		cat /etc/vpn/disconnect_pptp$i > /tmp/vpn/down_pptp$i 2>&1 | logger -t "net-set $0[$$]"
        chmod a+x /tmp/vpn/down_pptp$i
	fi
done
if [ ! -f /tmp/vpn/routeinfo_pptp1 ]; then
	echo "File /tmp/vpn/routeinfo_pptp1 not found. Writing default settings" | logger -t "net-set $0[$$]"
	echo "95.172.48.30 gw " > /tmp/vpn/routeinfo_pptp1
fi
if [ ! -f /tmp/vpn/routeinfo_pptp2 ]; then
	echo "File /tmp/vpn/routeinfo_pptp2 not found. Writing default settings" | logger -t "net-set $0[$$]"
	echo " gw " > /tmp/vpn/routeinfo_pptp2
fi

prior=`echo $prior | sed "s/sim1//"`
if [ ! -f /etc/ppp/CCID2 ] #if no CCID file
then
	/mnt/scripts/net_utils/rstmdm.sh sim2 > /dev/null 2>&1
	/mnt/scripts/net_utils/read_mdm.x -s 115200 -p /dev/ttyS1 -c AT+CCID -t 0 | grep -v "OK" | grep -v "AT" | grep -v "^[[:space:]]*$" > /etc/ppp/CCID2 #read CCID2
fi

ret=FAULT

#test connection to service
if [ "$working_channel" != "NULL" ]
then
	echo "Start checking db server. Working channel is $working_channel" | logger -t "net-set $0[$$]"
	for a in 1 2 3
	do
		if ping -c 1 10.0.0.2 > /dev/null 2>&1  ; then
			echo "Db server 10.0.0.2 pings OK. Exiting." | logger -t "net-set $0[$$]"
			ret=OK
			/mnt/scripts/led_state.sh 3 &
			break
		fi
	done
else
	echo "Working channel is NULL. Start choosing channel" | logger -t "net-set $0[$$]"
fi

#exit if ping is OK

if [ $ret = OK ];then
	/mnt/scripts/ntpdate.sh 10.0.1.1 | logger -t "net-set $0[$$]" &
	exit 0
else
	echo "Db server 10.0.0.2 ping FAILED. Trying to change channel." | logger -t "net-set $0[$$]"
fi
#check connections
for connect in $prior ; do
	sed -i "1 s/.*/$connect/g" /tmp/netsh.dat
	if [ $connect = eth0 ] ; then
		
		#TODO add pppoe connection

		#test connection channel
		ret=FAULT
		if [ $ret = FAULT ] ; then
			echo "Connection name:$connect. Ping server: 95.172.48.30 and  FAILED" | logger -t "net-set $0[$$]"
			continue
		fi
		if [ $ret = OK ] ; then
			/mnt/scripts/net_utils/connectpptp.sh &&  {
				ret=OK
			} || {
				ret=FAULT
			}
		else
			ret=FAULT
		fi
		echo "Connection name:$connect connect $ret" | logger -t "net-set $0[$$]" 
		#test connection to service
		/mnt/scripts/ntpdate.sh 10.0.1.1 | logger -t "net-set $0[$$]" &
		echo "Start checking db server." | logger -t "net-set $0[$$]"
		for a in 1 2 3
		do
			if ping -c 1 10.0.0.2 > /dev/null 2>&1; then
				echo "Db server 10.0.0.2 pings OK. Exiting." | logger -t "net-set $0[$$]"
				/mnt/scripts/led_state.sh 3 &
				sed -i "3 s/.*/$connect/g" /tmp/netsh.dat
				sed -i "2 s/.*/`date`/g" /tmp/netsh.dat
				exit 0
			fi
		done
		echo "Connection name:$connect. Db server 10.0.0.2 ping faild. Should try another channel." | logger -t "net-set $0[$$]"
		continue
		
			#TODO if DNS not set should set ppp DNS
			#rm /etc/resolv.conf  
			#ln -s /etc/ppp/resolv.conf /etc/resolv.conf 
	fi
	if [ $connect = sim1 ] ; then
		num=1
	fi
	if [ $connect = sim2 ] ; then
		num=2
	fi
	ret=FAULT
	#test if ppp is up
	ifconfig | grep ppp0 > /dev/null 2>&1 && {
		if [ "$connect" == "$working_channel" ]
		then
			for a in 1 2 3
			do
				 if ping -c 1 95.172.48.30 > /dev/null 2>&1 ; then
					echo "$connect checkikng pings to 95.172.48.30 OK." | logger -t "net-set $0[$$]"
					ret=OK
					/mnt/scripts/led_state.sh 2 &
					break
				fi
			done
		else
			echo "Current channel is $connect and working channel is $working_channel. Resetting ppp0." | logger -t "net-set $0[$$]"
			#kill conection
			set 0; set `ps | grep gprs | grep -v grep | grep -v "$0[$$]"` > /dev/null; test $1 -eq 0 || kill $1
			echo "`date +\"%D %T\"` $0[$$]: killed process PID:$1" | logger -t "net-set $0[$$]"
		fi
	}
	
	if [ $ret = FAULT ]
	then
		echo "$connect checkikng pings Failed." | logger -t "net-set $0[$$]"
		#сбрасываем модем и подключаемся заново
		/mnt/scripts/net_utils/rstmdm.sh sim$num
		/mnt/scripts/net_utils/connectgprs.sh gprs$num && {
			ret=OK
			/mnt/scripts/led_state.sh 2 & 
		} || {
			/mnt/scripts/led_state.sh 1 &
			echo "Connection name:$connect connect $ret" | logger -t "net-set $0[$$]"
			continue
		}			
		echo "Connection name:$connect connect $ret" | logger -t "net-set $0[$$]"
	fi
	if [ $ret = OK ]
	then
		/mnt/scripts/net_utils/connectpptp.sh && ret=OK 
		echo "Connection name:pptp connect $ret" | logger -t "net-set $0[$$]"
	fi 
	echo "Starting ntpdate." | logger -t "net-set $0[$$]"	
	/mnt/scripts/ntpdate.sh 10.0.1.1 | logger -t "net-set $0[$$]" &
	#test connection to service
	echo "Start checking db server." | logger -t "net-set $0[$$]"
	for a in 1 2 3
	do
		if ping -c 1 10.0.0.2 > /dev/null 2>&1; then
			echo "Db server 10.0.0.2 pings OK. Exiting." | logger -t "net-set $0[$$]"
			/mnt/scripts/led_state.sh 3 &
			sed -i "3 s/.*/$connect/g" /tmp/netsh.dat
			sed -i "2 s/.*/`date`/g" /tmp/netsh.dat
			exit 0
		fi
	done
	echo "Connection name:$connect. Db server 10.0.0.2 ping failed. Should try another channel." | logger -t "net-set $0[$$]"
	#kill conection
	set 0; set `ps | grep gprs$num | grep -v grep | grep -v "$0[$$]"` > /dev/null; test $1 -eq 0 || kill $1
	echo "`date +\"%D %T\"` $0[$$]: killed process PID:$1" | logger -t "net-set $0[$$]"
	continue
	
	
done
echo NULL > /tmp/netsh.dat
echo "`date`" >> /tmp/netsh.dat
echo NULL >> /tmp/netsh.dat
echo "No working channels. Exiting net.sh" | logger -t "net-set $0[$$]"
exit 0
