#!/bin/sh
#Auto reboot IC
UPTIME=172800		#uptime in sec. before reboot ic
if [ `awk '{printf("%d", $1)}' /proc/uptime` -ge $UPTIME ];
then
	/mnt/scripts/reboot
fi 