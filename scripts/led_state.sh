#!/bin/sh

selfname=${0##*/}
kill -9 `pidof -o $$ $selfname` 2>/dev/null
led="/mnt/scripts/pins.sh 152 1"

case "$1" in
	1)
		while [ 1 ] 
		do
			$led 1 > /dev/null
			sleep 2
			$led 0 > /dev/null
			usleep 50000
		done
	;;
	2) 
		while [ 1 ] 
		do
			$led 1 > /dev/null
			sleep 2
			$led 0 > /dev/null
			usleep 50000
			$led 1 > /dev/null
			usleep 100000
			$led 0 > /dev/null
			usleep 50000
		done
	;;
	*) 
		while [ 1 ] 
		do
			$led 1 > /dev/null
			sleep 2
			$led 0 > /dev/null
			usleep 50000
			$led 1 > /dev/null
			usleep 100000
			$led 0 > /dev/null
			usleep 50000
			$led 1 > /dev/null
			usleep 100000
			$led 0 > /dev/null
			usleep 50000
		done
	;;
esac