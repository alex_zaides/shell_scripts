#!/bin/sh
# Copy setting DB to flash
cp /mnt/www/ControlServer/data/ic_data3.sdb /mnt/www/ControlServer/data2/
# Backup sample DB
# -s - soft backup, -h = hard backup, else - no sample DB backup
if [[ "$1" == "-s" ]] ; then
	/mnt/scripts/backup_db.sh
elif [[ "$1" == "-h" ]] ; then
	cp /mnt/www/ControlServer/data/ic_data_value3.sdb /forsrv/backup/
fi
# Sync flash
sync