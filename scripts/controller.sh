#!/bin/sh

#find substring in string
# $1 - substring, $2..$n - string 
strstr () {
	#delete firs argument from string
	out=`echo $@ | sed "s/$1//"` 
	#find substing in string
	echo $out | grep -w $1 > /dev/null && return 0
	return 1
}

######### Information section
if [ $# -eq 0 ] ; then
    echo "Adapter of controller v1.0"
    echo "Usage: $0 [-hpaich] [FILE] {params}"
    echo "Options:"
    echo "      -i FILE     Controller initialization. FILE can be .cfg - text configurtionfile or .php - data base endpoint"
    exit 0;
fi

######### Initialization section
if [ $1 = "-i" ] ; then
    #pattern params string
    PATTERN_STR="prior ip gw dns1 dns2 mask mac ntp ppp_gprs1 ppp_gprs2 ppp_apn1 ppp_apn2 ppp_lgn1 ppp_lgn2 ppp_psw1 ppp_psw2 ppp_port ppp_baud ppp_check vpn_use vpn_type vpn_lgn1 vpn_lgn2 vpn_psw1 vpn_psw2 vpn_srv1 vpn_srv2 vpn_port1 vpn_port2 db_srv time_zone pppoe_use pppoe_type pppoe_srv pppoe_lgn pppoe_psw"
    #exit if config fail was skiped
    test $# -lt 2 && { echo "Error inpt params. Missing config file"; exit 1; }
    #Check configuration file. FILE can be .cfg - text configurtionfile or .php - data base endpoint
	case "${2##*.}" in
		cfg)           PARAMS_STR=`cat $2`  ;;     			#read config to string
		php)           PARAMS_STR=`php $2`  ;;
		*)             echo "Configuration file is in invalid format (.cfg or .php)"
					   exit ;;
	esac
    PREV_PARRM="NULL"

      #fill params
      for param in $PARAMS_STR
      do
        #echo "PARAM: $param"
		#echo "PREV_PARAM: $PREV_PARRM"
        strstr $param $PATTERN_STR && { PREV_PARRM=$param ; continue; }
        export `echo $PREV_PARRM`=$param
        echo "$PREV_PARRM = $param"
        PREV_PARRM="NULL"
      done

	#set eth0 IP settengs
	echo "updating eth0 settings" | logger -t "net-set $0[$$]"
	/mnt/scripts/net_utils/eth_config.sh $ip $mask $mac $gw | logger -t "net-set $0[$$]"
	
	#set DNS
	printenv | grep "dns1" > /dev/null && echo "nameserver $dns1" > /etc/resolv.conf
	printenv | grep "dns2" > /dev/null && echo "nameserver $dns2" >> /etc/resolv.conf
	echo "DNS1 was set to $dns1 and DNS2 was set to $dns2 in /etc/resolv.conf" | logger -t "net-set $0[$$]"
		
	rm -rf /etc/ppp/*connect*
	rm -rf /etc/ppp/ip*
	rm -rf /etc/ppp/*secrets
	rm -rf /etc/ppp/peers
	rm -rf /etc/ppp/checkmodem.sh
	rm -rf /etc/ppp/resolv.conf
	rm -rf /etc/ppp/CCID*
	rm -rf /etc/ppp/CSQ*
	rm -rf /etc/ppp/slots
		
	rm -rf /etc/vpn/*
	rm -rf /tmp/vpn/*
	rm -rf /tmp/ppp/*
	
	mkdir -p /etc/ppp/peers
	mkdir -p /etc/vpn
	
	
	rm -f /tmp/netsh.dat 
 
	#set VPN pptp settings for eth0
	echo "host=\`cat /tmp/vpn/routeinfo_pptp1\`" > /etc/vpn/connect_pptp1
	echo "route add -host \$host" >> /etc/vpn/connect_pptp1
	if [ -z "$vpn_srv1" ]
	then
		echo 'echo "No vpn server configured" | logger -t "net-set $0[$$]"' >> /etc/vpn/connect_pptp1
	fi
	echo "exit 0" >> /etc/vpn/connect_pptp1
	
	echo "host=\`cat /tmp/vpn/routeinfo_pptp2\`" > /etc/vpn/connect_pptp2
	echo "route add -host \$host" >> /etc/vpn/connect_pptp2
	if [ -z "$vpn_srv2" ]
	then
		echo 'echo "No vpn server configured" | logger -t "net-set $0[$$]"' >> /etc/vpn/connect_pptp2
	fi
	echo "exit 0" >> /etc/vpn/connect_pptp2

	touch /etc/ppp/slots
	if [ $ppp_gprs1 = "1" ];then
		echo 'sim1' >> /etc/ppp/slots
	fi
	if [ $ppp_gprs2 = "1" ];then
		echo 'sim2' >> /etc/ppp/slots
	fi
		

	cat > /etc/ppp/ip-up <<!
#!/bin/sh
echo \$1
echo \$5
route del default
route add -net default gw \$5 dev \$1
pid=\`cat /var/log/\$1.pid\`
connection=\`ps | grep \$pid | grep -v grep | grep pppd\`
connection=\`echo \${connection##* }\`
echo "\$connection \$1 started - OK" > /tmp/ppp/started
!
if  [ $vpn_use = "1" ] ; then
	cat >> /etc/ppp/ip-up <<!
if [ \$1 = ppp0 ];then
	sed -i "/exit 0/ d" /tmp/vpn/down_pptp1
	echo "route add -net default gw \$5 dev \$1" >> /tmp/vpn/down_pptp1
	echo "exit 0" >> /tmp/vpn/down_pptp1
	sed -i "/exit 0/ d" /tmp/vpn/down_pptp2
	echo "route add -net default gw \$5 dev \$1" >> /tmp/vpn/down_pptp2
	echo "exit 0" >> /tmp/vpn/down_pptp2
	echo "$vpn_srv1 dev ppp0" > /tmp/vpn/routeinfo_pptp1
	echo "$vpn_srv2 dev ppp0" > /tmp/vpn/routeinfo_pptp2
	#sed -i  "s/route add -host $vpn_srv1 .\+/route add -host $vpn_srv1 dev ppp0/" /etc/vpn/connect_pptp1
	#sed -i  "s/route add -host $vpn_srv2 .\+/route add -host $vpn_srv2 dev ppp0/" /etc/vpn/connect_pptp2
else
	current_channel="\`head -n 1 /tmp/netsh.dat\`"
	if [ "\$current_channel" == "eth0" ] ; then
		sed -i "/exit 0/ d" /tmp/vpn/down_pptp1
		echo "route add -net default gw $gw" >> /tmp/vpn/down_pptp1
		echo "exit 0" >> /tmp/vpn/down_pptp1
		sed -i "/exit 0/ d" /tmp/vpn/down_pptp2
		echo "route add -net default gw $gw" >> /tmp/vpn/down_pptp2
		echo "exit 0" >> /tmp/vpn/down_pptp2
	fi
fi
!
fi
cat >> /etc/ppp/ip-up <<!
echo "pppd:\$1 started OK." | logger -t "net-set \$0[\$$]"
!

	cat > /etc/ppp/ip-down <<!
#!/bin/sh
if [ \$1 = ppp0 ];then
	route del default
	route add -net default gw $gw dev eth0
	/mnt/scripts/led_state.sh 1 &
!
if  [ $vpn_use = "1" ] ; then
	cat >> /etc/ppp/ip-down <<!
	sed	-i "/route add -net default gw.\+/ d" /tmp/vpn/down_pptp1
	sed	-i "/route add -net default gw.\+/ d" /tmp/vpn/down_pptp2
	echo "$vpn_srv1 gw $gw" > /tmp/vpn/routeinfo_pptp1
	echo "$vpn_srv2 gw $gw" > /tmp/vpn/routeinfo_pptp2
	#sed -i "s/route add -host $vpn_srv1 .\+/route add -host $vpn_srv1 gw $gw/" /etc/vpn/connect_pptp1
	#sed -i "s/route add -host $vpn_srv2 .\+/route add -host $vpn_srv2 gw $gw/" /etc/vpn/connect_pptp2
else #if pptp downed
	current_channel="\`head -n 1 /tmp/netsh.dat\`"
	if [ "\$current_channel" == "eth0" ] ; then
		/mnt/scripts/led_state.sh 1 &	
	else
		/mnt/scripts/led_state.sh 2 &
	fi
	current_connection=\`awk '{print \$1}' /tmp/ppp/started \`
	echo "pptp is disconnrcted now. current connectioon is \$current_connection" | logger -t "net-set \$0[\$$]"
	/tmp/vpn/down_\$current_connection
!
fi
cat >> /etc/ppp/ip-down <<!
fi
echo "pppd:\$1 disconnected" | logger -t "net-set \$0[\$$]"

!
	
   
    
	for i in "1" "2" ; do
    #set login and password
		
		
		#if [ $$R = "1" ] ; then
			ppp_lgn_t=`echo ppp_lgn$i`
			ppp_psw_t=`echo ppp_psw$i`
			eval ppp_lgn_t=\$$ppp_lgn_t
			eval ppp_psw_t=\$$ppp_psw_t
			echo "$ppp_lgn_t gprs$i $ppp_psw_t " >> /etc/ppp/pap-secrets
			#create connect script
			ppp_apn_t=`echo ppp_apn$i`
			eval ppp_apn_t=\$$ppp_apn_t
			cat > /etc/ppp/connect$i <<!
TIMEOUT 60
ABORT 'BUSY'
ABORT 'ERROR'
ABORT 'NO CARRIER'
SAY "\n + go go go"
'' 'AT'
OK 'AT+CCID'
OK 'AT+CSQ'
OK AT+CGDCONT=1,"IP","$ppp_apn_t"
SAY "\n + defining PDP context"
OK 'AT+CGATT=1'
SAY "<br> + attaching to GPRS"
OK 'ATD*99***1#'
SAY "<br> + requesting data connection"
CONNECT '\176\377\175\43\300\41\175\41\175\40\175\40\175\67\175\42\175\46\175\40\175\40\175\40\175\40\175\45\175\46\74\75\110\203\175\47\175\42\175\50\175\42\175\55\175\43\175\46\60\121\176'
SAY "<br> + connected"
!
			#create disconnect script
			cat > /etc/ppp/disconnect$i <<!
ABORT 'BUSY'
ABORT 'ERROR'
ABORT 'NO DIALTONE'
TIMEOUT 30
'' '+++\c'
SAY " + sending break"
'NO CARRIER' 'ATH'
SAY "\n + dropping data connection"
OK 'AT+CGATT=0'
SAY "\n + disconnecting from GPRS"
OK '\c'
SAY "\n + disconnected." 			
!
			#create pppd option file for ppp0 network interface
			cat > /etc/ppp/peers/gprs$i <<!
/dev/$ppp_port
$ppp_baud
debug
defaultroute
updetach
mtu 1400
noipdefault
ipparam gprs$i
user "$ppp_lgn_t"
ipcp-accept-local
ipcp-accept-remote
novj
novjccomp
usepeerdns
unit 0
connect "chat -v -f /etc/ppp/connect$i"
disconnect "chat -v -f /etc/ppp/disconnect$i"
!
		#fi
	done
		#create checkmodem.sh
		cat > /etc/ppp/checkmodem.sh <<!
#!/bin/sh
#check modem port
cnt=0
while [ "\$cnt" -le 3 ] ; do
    count=1
    /mnt/scripts/net_utils/rstmdm.sh \$1 > /dev/null 2>&1
    while [ "\$count" -le 5 ] ; do
        /mnt/scripts/net_utils/read_mdm.x -s $ppp_baud -p /dev/$ppp_port -c AT+CCID -t 0 | grep "OK" > /dev/null 2>&1 && {
            echo "AT - OK" | logger -t "net-set \$0[\$$]"
            #Read SIM CCID
            /mnt/scripts/net_utils/read_mdm.x -s $ppp_baud -p /dev/$ppp_port -c AT+CCID -t 0 | grep -v "OK" | grep -v "AT" | grep -v "^[[:space:]]*$" > /etc/ppp/CCID\`echo \$1 | sed "s/sim//"\` 
            #Read CSQ
			sleep 15		
            csq=\`/mnt/scripts/net_utils/read_mdm.x -s $ppp_baud -p /dev/$ppp_port  -c AT+CSQ -t 0 | grep +CSQ:\`
            echo "Signal quality: \$csq" | logger -t "net-set \$0[\$$]"
            if [ -n "$csq" ]
            then
            	echo "\$csq" > /etc/ppp/CSQ\`echo \$1 | sed "s/sim//"\`
			fi
            exit 0
        }
        #printf "step:\$cnt:\$count\r" 
        let "count += 1"
    done
    echo "AT - FAILED" | logger -t "net-set \$0[\$$]"
    let "cnt += 1"
done
exit 1
!
##		#gprs connection checking
##		grep -v "needreboot" /var/spool/cron/crontabs/root | grep -v "SHELL=/bin/sh" > /var/spool/cron/crontabs/tmp
##		echo "SHELL=/bin/sh" > /var/spool/cron/crontabs/root
##		echo "*/25    *       *       *       *       if ifconfig | grep "ppp"; then rm /needreboot ; else if grep 1 /needreboot ; then /mnt/scripts/toflash.sh ; reboot ; else echo 1 > /needreboot ; fi ; fi" >> /var/spool/cron/crontabs/root
##		cat /var/spool/cron/crontabs/tmp >> /var/spool/cron/crontabs/root
##		killall crond; crond &


	if [ $vpn_use = "1" ] ; then
		if [ $vpn_type = "pptp" ] ; then
			#set VPN pptp settings for gprs
			for i in 1 2 ; do
				#create vpn connect script
				vpn_srv_t=`echo vpn_srv$i`
				eval vpn_srv_t=\$$vpn_srv_t
				#create vpn disconnect script
				echo "route del -host $vpn_srv_t" > /etc/vpn/disconnect_pptp$i
				echo "echo \"pptp$i disconnected\" | logger -t \"net-set \$0[\$$]\"" >> /etc/vpn/disconnect_pptp$i
				echo "exit 0" >> /etc/vpn/disconnect_pptp$i
				#create vpn chap-secrets file
				vpn_lgn_t=`echo vpn_lgn$i`
				vpn_psw_t=`echo vpn_psw$i`
				eval vpn_lgn_t=\$$vpn_lgn_t
				eval vpn_psw_t=\$$vpn_psw_t
				echo "$vpn_lgn_t pptp$i $vpn_psw_t * " >> /etc/ppp/chap-secrets
				#create pptp option file for ppp1 network interface
				cat > /etc/ppp/peers/pptp$i <<!
remotename pptp$i
pty "pptp --loglevel 2 $vpn_srv_t --nolaunchpppd"
name $vpn_lgn_t
noauth
debug
updetach
unit 1
lock
defaultroute
noccp
connect /etc/vpn/connect_pptp$i
disconnect /etc/vpn/disconnect_pptp$i
!
			done
		fi
	fi

cat > /mnt/scripts/net.sh <<!
#Create net.sh script
#Check if net.sh already running

if [ \`ps | grep -v '/bin/sh'| grep -c 'net.s[h]'\` -gt 2 ]
then
	echo "Try starting net.sh. Already running" | logger -t "net-set \$0[\$$]"
	exit 0
fi


prior=\`echo '$prior' | sed 's/;/ /g'\`
if [ ! -f /tmp/netsh.dat ]; then
	echo "File /tmp/netsh.dat not found. Writing default settings" | logger -t "net-set \$0[\$$]"
	echo NULL > /tmp/netsh.dat			#current channel try to connect
	echo "\`date\`" >> /tmp/netsh.dat	#time working channel was set
	echo NULL >> /tmp/netsh.dat			#last working channel
fi

mkdir -p /tmp/ppp

#get current channel
working_channel="\`head -n 3 /tmp/netsh.dat | tail -n 1\`"

for i in "1" "2"
do
	if [ ! -f /tmp/vpn/down_pptp\$i ]; then
		echo "File /tmp/vpn/down_pptp\$i not found. Copy from /etc/vpn/disconnect_pptp\$i" | logger -t "net-set \$0[\$$]"
		mkdir -p /tmp/vpn
		cat /etc/vpn/disconnect_pptp\$i > /tmp/vpn/down_pptp\$i 2>&1 | logger -t "net-set \$0[\$$]"
        chmod a+x /tmp/vpn/down_pptp\$i
	fi
done
if [ ! -f /tmp/vpn/routeinfo_pptp1 ]; then
	echo "File /tmp/vpn/routeinfo_pptp1 not found. Writing default settings" | logger -t "net-set \$0[\$$]"
	echo "$vpn_srv1 gw $gw" > /tmp/vpn/routeinfo_pptp1
fi
if [ ! -f /tmp/vpn/routeinfo_pptp2 ]; then
	echo "File /tmp/vpn/routeinfo_pptp2 not found. Writing default settings" | logger -t "net-set \$0[\$$]"
	echo "$vpn_srv2 gw $gw" > /tmp/vpn/routeinfo_pptp2
fi

!

if [ $ppp_gprs1 = "0" ];then
	cat >> /mnt/scripts/net.sh <<!
prior=\`echo \$prior | sed "s/sim1//"\`
!
else
	cat >> /mnt/scripts/net.sh <<!
if [ ! -f /etc/ppp/CCID1 ] #if no CCID file
then
	/mnt/scripts/net_utils/rstmdm.sh sim1 > /dev/null 2>&1
	/mnt/scripts/net_utils/read_mdm.x -s $ppp_baud -p /dev/$ppp_port -c AT+CCID -t 0 | grep -v "OK" | grep -v "AT" | grep -v "^[[:space:]]*$" > /etc/ppp/CCID1 #read CCID1
fi
!
fi

if [ $ppp_gprs2 = "0" ];then
	cat >> /mnt/scripts/net.sh <<!
prior=\`echo \$prior | sed "s/sim2//"\`
!
else
	cat >> /mnt/scripts/net.sh <<!
if [ ! -f /etc/ppp/CCID2 ] #if no CCID file
then
	/mnt/scripts/net_utils/rstmdm.sh sim2 > /dev/null 2>&1
	/mnt/scripts/net_utils/read_mdm.x -s $ppp_baud -p /dev/$ppp_port -c AT+CCID -t 0 | grep -v "OK" | grep -v "AT" | grep -v "^[[:space:]]*$" > /etc/ppp/CCID2 #read CCID2
fi
!
fi

cat >> /mnt/scripts/net.sh <<!

ret=FAULT

#test connection to service
if [ "\$working_channel" != "NULL" ]
then
	echo "Start checking db server. Working channel is \$working_channel" | logger -t "net-set \$0[\$$]"
	for a in 1 2 3
	do
		if ping -c 1 $db_srv > /dev/null 2>&1  ; then
			echo "Db server $db_srv pings OK. Exiting." | logger -t "net-set \$0[\$$]"
			ret=OK
			/mnt/scripts/led_state.sh 3 &
			break
		fi
	done
else
	echo "Working channel is NULL. Start choosing channel" | logger -t "net-set \$0[\$$]"
fi

#exit if ping is OK

if [ \$ret = OK ];then
	/mnt/scripts/ntpdate.sh $ntp | logger -t "net-set \$0[\$$]" &
	exit 0
else
	echo "Db server $db_srv ping FAILED. Trying to change channel." | logger -t "net-set \$0[\$$]"
fi
#check connections
for connect in \$prior ; do
	sed -i "1 s/.*/\$connect/g" /tmp/netsh.dat
	if [ \$connect = eth0 ] ; then
		
		#TODO add pppoe connection

		#test connection channel
		ret=FAULT
!
		if [ -n "$gw" ] ; then

			if [ "$gw" != "$db_srv" ] && [ "$gw" != "$vpn_srv1" ] || [ "$gw" != "$vpn_srv2" ] ; then
				cat >> /mnt/scripts/net.sh <<!
				route add -host $ppp_check gw $gw > /dev/null 2>&1 
				for a in 1 2 3 ; do
				ping -c 1 $ppp_check > /dev/null 2>&1 && {
					ret=OK
					echo "Connection name:\$connect. Ping server: $ppp_check OK" | logger -t "net-set \$0[\$$]"
					/mnt/scripts/led_state.sh 2 &
					break
					} || {
					ping -c 1 $gw > /dev/null 2>&1 && {
						ret=OK 
						echo "Connection name:\$connect. Ping server: $gw OK" | logger -t "net-set \$0[\$$]"
						/mnt/scripts/led_state.sh 2 &
						break
					}
				}
					
				#TODO check if inet pings throw this channel and kill all ppp if it is so
				done
				route del $ppp_check > /dev/null 2>&1
!
			fi
		fi
		
		cat >> /mnt/scripts/net.sh <<!		
		if [ \$ret = FAULT ] ; then
			echo "Connection name:\$connect. Ping server: $ppp_check and $gw FAILED" | logger -t "net-set \$0[\$$]"
			continue
		fi
!

if [ $vpn_use = "1" ]; then
cat >> /mnt/scripts/net.sh <<!
		if [ \$ret = OK ] ; then
			/mnt/scripts/net_utils/connectpptp.sh &&  {
				ret=OK
			} || {
				ret=FAULT
			}
		else
			ret=FAULT
		fi
		echo "Connection name:\$connect connect \$ret" | logger -t "net-set \$0[\$$]" 
!
fi
		cat >> /mnt/scripts/net.sh <<!	
		#test connection to service
		/mnt/scripts/ntpdate.sh $ntp | logger -t "net-set \$0[\$$]" &
		echo "Start checking db server." | logger -t "net-set \$0[\$$]"
		for a in 1 2 3
		do
			if ping -c 1 $db_srv > /dev/null 2>&1; then
				echo "Db server $db_srv pings OK. Exiting." | logger -t "net-set \$0[\$$]"
				/mnt/scripts/led_state.sh 3 &
				sed -i "3 s/.*/\$connect/g" /tmp/netsh.dat
				sed -i "2 s/.*/\`date\`/g" /tmp/netsh.dat
				exit 0
			fi
		done
		echo "Connection name:\$connect. Db server $db_srv ping faild. Should try another channel." | logger -t "net-set \$0[\$$]"
		continue
		
			#TODO if DNS not set should set ppp DNS
			#rm /etc/resolv.conf  
			#ln -s /etc/ppp/resolv.conf /etc/resolv.conf 
	fi
	if [ \$connect = sim1 ] ; then
		num=1
	fi
	if [ \$connect = sim2 ] ; then
		num=2
	fi
	ret=FAULT
	#test if ppp is up
	ifconfig | grep ppp0 > /dev/null 2>&1 && {
		if [ "\$connect" == "\$working_channel" ]
		then
			for a in 1 2 3
			do
				 if ping -c 1 $ppp_check > /dev/null 2>&1 ; then
					echo "\$connect checkikng pings to $ppp_check OK." | logger -t "net-set \$0[\$$]"
					ret=OK
					/mnt/scripts/led_state.sh 2 &
					break
				fi
			done
		else
			echo "Current channel is \$connect and working channel is \$working_channel. Resetting ppp0." | logger -t "net-set \$0[\$$]"
			#kill conection
			set 0; set \`ps | grep gprs | grep -v grep | grep -v "\$0[\$$]"\` > /dev/null; test \$1 -eq 0 || kill \$1
			echo "\`date +\"%D %T\"\` \$0[\$$]: killed process PID:\$1" | logger -t "net-set \$0[\$$]"
		fi
	}
	
	if [ \$ret = FAULT ]
	then
		echo "\$connect checkikng pings Failed." | logger -t "net-set \$0[\$$]"
		#сбрасываем модем и подключаемся заново
		/mnt/scripts/net_utils/rstmdm.sh sim\$num
		/mnt/scripts/net_utils/connectgprs.sh gprs\$num && {
			ret=OK
			/mnt/scripts/led_state.sh 2 & 
		} || {
			/mnt/scripts/led_state.sh 1 &
			echo "Connection name:\$connect connect \$ret" | logger -t "net-set \$0[\$$]"
			continue
		}			
		echo "Connection name:\$connect connect \$ret" | logger -t "net-set \$0[\$$]"
	fi
!
	
if [ $vpn_use = "1" ]; then
cat >> /mnt/scripts/net.sh <<!
	if [ \$ret = OK ]
	then
		/mnt/scripts/net_utils/connectpptp.sh && ret=OK 
		echo "Connection name:pptp connect \$ret" | logger -t "net-set \$0[\$$]"
	fi 
!
fi
			cat >> /mnt/scripts/net.sh <<!
	echo "Starting ntpdate." | logger -t "net-set \$0[\$$]"	
	/mnt/scripts/ntpdate.sh $ntp | logger -t "net-set \$0[\$$]" &
	#test connection to service
	echo "Start checking db server." | logger -t "net-set \$0[\$$]"
	for a in 1 2 3
	do
		if ping -c 1 $db_srv > /dev/null 2>&1; then
			echo "Db server $db_srv pings OK. Exiting." | logger -t "net-set \$0[\$$]"
			/mnt/scripts/led_state.sh 3 &
			sed -i "3 s/.*/\$connect/g" /tmp/netsh.dat
			sed -i "2 s/.*/\`date\`/g" /tmp/netsh.dat
			exit 0
		fi
	done
	echo "Connection name:\$connect. Db server $db_srv ping failed. Should try another channel." | logger -t "net-set \$0[\$$]"
	#kill conection
	set 0; set \`ps | grep gprs\$num | grep -v grep | grep -v "\$0[\$$]"\` > /dev/null; test \$1 -eq 0 || kill \$1
	echo "\`date +\"%D %T\"\` \$0[\$$]: killed process PID:\$1" | logger -t "net-set \$0[\$$]"
	continue
	
	
done
echo NULL > /tmp/netsh.dat
echo "\`date\`" >> /tmp/netsh.dat
echo NULL >> /tmp/netsh.dat
echo "No working channels. Exiting net.sh" | logger -t "net-set \$0[\$$]"
exit 0
!
	chmod a+x /etc/ppp/*connect*
	chmod a+x /etc/ppp/checkmodem.sh
	chmod a+x /etc/ppp/ip-up
	chmod a+x /etc/ppp/ip-down
	chmod a+x /etc/vpn/*
	chmod a+x /mnt/scripts/net.sh
	sync
	
    exit 0
fi
