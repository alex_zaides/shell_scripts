#!/bin/sh
#Source DB
db="/mnt/www/ControlServer/data/ic_data_value3.sdb"
#Destination DB
bkpdb="/forsrv/backup/ic_data_value3.sdb"
#Backup DB
echo -e ".timeout 30000\n.backup /$bkpdb.tmp" | sqlite3 $db > /dev/null 2>&1 && \
echo "vacuum;" | sqlite3 $bkpdb.tmp > /dev/null 2>&1 && \
ln -f $bkpdb.tmp $bkpdb > /dev/null 2>&1 && rm -f $bkpdb.tmp > /dev/null 2>&1