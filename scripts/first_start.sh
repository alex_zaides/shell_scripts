#!/bin/sh

#Once initial configuration script


STAMP_FILE=/mnt/scripts/first_start.stamp

if [[ -f $STAMP_FILE ]]; then
	exit 0
fi

touch $STAMP_FILE

rm /var/run
ln -s /tmp /var/run

kill -STOP `pidof -s getty` 2>/dev/null

echo -e \\033c > /dev/ttyS0 #Reset terminal
echo "********* Ending controller firmware..."
echo "********* Please DO NOT TURN OFF THE POWER and enter commands!!!"

# MSP
echo "********* Starting MSP flashing..."
/mnt/msp/bsl.IKv3 /mnt/msp/script.txt \
					| tail | grep 'FAIL writing data block starting at fffe' > /dev/null 2>&1 \
					|| /mnt/msp/bsl.IKv3 /mnt/msp/script.txt \
					| tail | grep 'FAIL writing data block starting at fffe' > /dev/null 2>&1 \
					|| echo "********* Unable to flash MSP for two attempts. Try manually reflash"
echo "********* End MSP flash"

# Device configuration
echo "********* Configuring device table..."
mdev -s

# Default network settings
echo "********* Configuring network settings..."
/mnt/scripts/controller.sh -i '/mnt/www/ControlServer/adapterService/getNetworkSettings.php' > /dev/null 2>&1

# Reading IMEI
echo "********* Reading IMEI from modem..."
/mnt/scripts/net_utils/read_mdm.x -s 115200 -p /dev/ttyS1 -c AT+GSN -t 0 | grep -v "OK" | grep -v "AT" | grep -v "^[[:space:]]*$" > /etc/ppp/IMEI

sync

echo "********* The controller has been successfully configured"

#User message
echo -e "\n\n***************************************************************************"
echo "********* To complete the firmware, DISCONNECT POWER from the controller!!!"
echo "***************************************************************************"

exit 1
