#!/bin/sh
DAT_FILE=/tmp/logmover.dat
filelist=$(cat /mnt/scripts/loglist.txt)
echo $filelist

for fname in $filelist
do
	echo "fname=$fname"
	bname=$(basename $fname)
	echo bname=$bname
	if [ $bname == "messages" ]
	then
		if [ -f "$fname.0" ] #if message.0 exist
		then
			msg_time=`date -r "$fname.0" +"%s"`
			echo msg_time=$msg_time
			if [ -f $DAT_FILE ] #if DAT_FILE exist
			then
				old_msg_time=`cat $DAT_FILE`
				echo old_msg_time=$old_msg_time
				if [ "$msg_time" -le "$old_msg_time" ]
				then
					echo "no new files to copy"
					continue
				fi
			fi #if DAT_FILE exist
			echo $msg_time > $DAT_FILE
			cat $fname.0 >> /logs/$bname
		fi #if message.0 exist
	else
		cat $fname >> /logs/$bname && echo "" > $fname
	fi
done

for fname in /logs/*
do
	echo $fname
	set `du $fname`
	filelen=$1
	if [ "$filelen" -ge 500 ]
	then
		echo "removing $fname"
		rm -f $fname.gz
		gzip $fname
		rm -f $fname
	fi
done

rm -f /mnt/data/sens*