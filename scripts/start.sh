#!/bin/sh
/bin/mount -o remount,noatime,rw /
mknod /dev/ppp c 108 0 > /dev/null 2>&1
rm /dev/ttyS4 > /dev/null 2>&1
mknod /dev/ttyS4 c 4 68

mkdir /tmp/php_data
ln -s /tmp/php_data /mnt/www/ControlServer/data
cp -r /mnt/www/ControlServer/data2/* /mnt/www/ControlServer/data/

/mnt/scripts/first_start.sh || exit 0

hwclock -l -s
/mnt/scripts/hwwdt.sh > /dev/null
/mnt/scripts/led_state.sh 1 &
echo "1" > /proc/sys/net/ipv4/ip_forward
lighttpd -f /mnt/config/lighttpd.conf &

/mnt/scripts/msp430rst.sh > /dev/null
insmod /lib/modules/2.6.36-rc6/kernel/drivers/mfd/controller.ko
php /mnt/www/ControlServer/cronService/Restorer.php

killall -q crond 
mkdir /var/spool/cron
mkdir /var/spool/cron/crontabs
cp /mnt/crontabs/* /var/spool/cron/crontabs/
crond

/mnt/scripts/net.sh &
/mnt/scripts/add_start.sh &
