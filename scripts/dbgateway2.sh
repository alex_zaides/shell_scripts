#!/bin/sh
#------------------ dbgateway ver.2 start script ------------------------
#----------------------------- settings ---------------------------------
APP=/mnt/dbgateway/dbgateway2				#Application file
PID=/var/run/dbgateway2.pid 				#Server pid file
SOCKET=/var/lock/dbgateway2.socket 			#Socket file
EVENT=/forsrv/event/ic_data_event3.sdb 			#Event DB file
VALUE=/mnt/www/ControlServer/data/ic_data_value3.sdb	#Value DB file
SAMP_REST=/forsrv/backup/			#Path to clean DB (for restore)
DB_INTERVAL=45						#Write to DB interval (sec.)
DB_LIFETIME=604800					#Lifetime of record in DB (sec.)
DB_TIMEOUT=15						#DB operation timeout (sec.)
BACKUP_INTERVAL=1800					#DB backup interval (sec.)
BACKUP_DIR=/forsrv/backup/ic_data_value3.sdb		#DB backup directory
AGE_LIMIT=1800						#Max age of data before pack to archive
RECORD_LIMIT=150					#Max record in archive
ARCH_PATH=/forsrv/archive/DataArch_%Y_%m_%d_%H_%M_%S.gz	#Archive file name
LOG=/var/log/messages				#Start script log file
DEBUG=0							#1-enable debug logging, 0-disable

#----------------------------- run server --------------------------------
START=0
if [ -e $PID ]; then
	PROC=`head -1 $PID`
	if [[ -z $PROC ]]; then
		echo "`date +\"%D %T\"` Another instance initialize... $PID" >> $LOG
		exit
	else
		if kill -0 $PROC ; then
	    	echo "`date +\"%D %T\"` Already running $PID" >> $LOG
	    	exit
		else
	        echo "`date +\"%D %T\"` Not running $PID, but pid file exists, probably died. Starting..." >> $LOG
	        rm $PID >> $LOG
			START=1
		fi
	fi
else
	echo "`date +\"%D %T\"` Not running $PID. Starting..." >> $LOG
	START=1
fi
if [ $START -eq 1 ]; then
	if [ -e $SOCKET ]; then
		rm $SOCKET >> $LOG
	fi
	if [ $DEBUG -eq 1 ]; then
		export DBGATEWAY2_LOG=all
	fi
	$APP \
		-S $SOCKET \
		-p $PID \
		-E $EVENT \
		-V $VALUE \
		--sample-db-rest $SAMP_REST \
		-F $DB_INTERVAL \
		-L $DB_LIFETIME \
		-T $DB_TIMEOUT \
		-I $BACKUP_INTERVAL \
		-B $BACKUP_DIR \
		-R $RECORD_LIMIT \
		-Z $ARCH_PATH \
		-A $AGE_LIMIT >> $LOG
fi
